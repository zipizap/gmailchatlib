package gmailchatlib

import (
	"crypto/tls"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/k0kubun/pp"
	xmpp "github.com/mattn/go-xmpp"
	"github.com/olebedev/config"
)

/*
	Hi curious mind!
	If you are reading this code, then it will probably be a good moment for me to explain a couple of details about it:
    - To hide some functions and variables as internal, I've added an 'i' prefix to their name
		- This lib is only a superficial-varnish-coat made on top of the great work of the original xmpp-go lib (taken from here: github.com/mattn/go-xmpp). So 90% of credit goes there, and 10% stays here :)
		- Have fun!
*/

// DEFAULT_XMPP_STATUSMESSAGE is the status that will appear in the chat
var DEFAULT_XMPP_STATUSMESSAGE = "Hi there"

// DEFAULT_YAML_FILE is the filepath to the yaml config
var DEFAULT_YAML_FILE = "./" + filepath.Base(os.Args[0]) + ".yaml"

var DEFAULT_LOGGER log.Logger
var DEFAULT_LOGGER_ENABLED = false
var DEFAULT_LOGGER_FILE = "./" + filepath.Base(os.Args[0]) + ".log"

var iDEFAULT_XMPP_STATUS = "xa"
// NOTLS true allows StartTls to happen and upgrade the connection from unencrypted to TLS-encrypted
var iDEFAULT_XMPP_NOTLS = true   

var iDEFAULT_XMPP_DEBUG = false
var iDEFAULT_XMPP_SESSION = true

type xmppData struct {
	server        *string
	username      *string
	password      *string
	status        *string
	statusMessage *string
	notls         *bool
	debug         *bool
	session       *bool
}

type iCfg struct {
	// .yaml config
	cfgYaml *config.Config

	// xData populated from cfgYaml
	xData *xmppData
}

var iCfgSingleton *iCfg

func init_iCfgSingleton() (*iCfg, error) {
	iCfgSingleton = &iCfg{}
	var err error

	// initLog()
	{
		initLog := func() error {
			var f io.Writer
			if DEFAULT_LOGGER_ENABLED == true {
				//create your log file with desired read/write permissions
				var err error
				f, err = os.OpenFile(DEFAULT_LOGGER_FILE, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
				// defer f.Close()
				if err != nil {
					//DEFAULT_LOGGER.Print("[ERROR] ", err)
					//NOTE: DEFAULT_LOGGER is not yet ready, we cant use it this time
					return err
				}
			} else {
				//f is like /dev/null, logs are discarded
				f = ioutil.Discard
			}
			DEFAULT_LOGGER = *log.New(f, "", log.Ldate|log.Ltime)
			return nil
		}
		err = initLog()
		if err != nil {
			DEFAULT_LOGGER.Print("[ERROR] ", err)
			return nil, err
		}
	}

	// iCfgSingleton.cfgYaml
	{
		iCfgSingleton.cfgYaml, err = config.ParseYamlFile(DEFAULT_YAML_FILE)
		if err != nil {
			DEFAULT_LOGGER.Print("[ERROR] ", err)
			return nil, err
		}
	}

	// iCfgSingleton.xData
	{
		// from defaults and cfgYaml, define xData
		iCfgSingleton.xData = &xmppData{
			// server
			// username
			// password
			status:        &iDEFAULT_XMPP_STATUS,
			statusMessage: &DEFAULT_XMPP_STATUSMESSAGE,
			notls:         &iDEFAULT_XMPP_NOTLS,
			debug:         &iDEFAULT_XMPP_DEBUG,
			session:       &iDEFAULT_XMPP_SESSION,
		}

		// xData.server
		{
			var tmp_str string
			tmp_str, err = iCfgSingleton.cfgYaml.String("account.server_and_port")
			if err != nil {
				DEFAULT_LOGGER.Print("[ERROR] ", err)
				return nil, err
			}
			iCfgSingleton.xData.server = &tmp_str
		}

		// xData.username
		{
			var tmp_str string
			tmp_str, err = iCfgSingleton.cfgYaml.String("account.username")
			if err != nil {
				DEFAULT_LOGGER.Print("[ERROR] ", err)
				return nil, err
			}
			iCfgSingleton.xData.username = &tmp_str
		}

		// xData.password
		{
			var tmp_str string
			tmp_str, err = iCfgSingleton.cfgYaml.String("account.password")
			if err != nil {
				DEFAULT_LOGGER.Print("[ERROR] ", err)
				return nil, err
			}
			iCfgSingleton.xData.password = &tmp_str
		}
		// ATP: iCfgSingleton.xData all defined and ready
	}

	DEFAULT_LOGGER.Print("================== Starting bot ======================")
	return iCfgSingleton, err
}

type iChatManager struct {
	talk             *xmpp.Client
	receivedChatChan chan xmpp.Chat
}

var iChatManagerSingleton *iChatManager

func init_iChatManagerSingleton() (*iChatManager, error) {
	// pre-requisites: iCfgSingleton ready
	{
		_, err := init_iCfgSingleton()
		if err != nil {
			DEFAULT_LOGGER.Print("[ERROR] ", err)
			return nil, err
		}
	}

	// init iChatManagerSingleton and its fields
	iChatManagerSingleton = &iChatManager{}
	// iChatManagerSingleton.talk
	{
		if !*(iCfgSingleton.xData.notls) {
			// *notls false
			xmpp.DefaultConfig = &tls.Config{
				ServerName:         strings.Split(*iCfgSingleton.xData.server, ":")[0],
				InsecureSkipVerify: false,
			}
		}

		options := xmpp.Options{
			Host:          *iCfgSingleton.xData.server,
			User:          *iCfgSingleton.xData.username,
			Password:      *iCfgSingleton.xData.password,
			NoTLS:         *iCfgSingleton.xData.notls,
			Debug:         *iCfgSingleton.xData.debug,
			Session:       *iCfgSingleton.xData.session,
			Status:        *iCfgSingleton.xData.status,
			StatusMessage: *iCfgSingleton.xData.statusMessage,
		}
		talk, err := options.NewClient()
		if err != nil {
			DEFAULT_LOGGER.Print("[ERROR] ", err)
			return nil, err
		}
		iChatManagerSingleton.talk = talk
	}
	// iChatManagerSingleton.receivedChatChan
	{
		iChatManagerSingleton.receivedChatChan = make(chan xmpp.Chat)
	}

	// ATP: iChatManagerSingleton is defined

	// Call additional methods
	{
		LaunchBaloon_ReceiveChatMessages := func(talk *xmpp.Client) {
			for {
				chat, err := talk.Recv()
				if err != nil {
					DEFAULT_LOGGER.Print("[ERROR] ", err)
					//return nil, err
				}
				switch chat.(type) {
				case xmpp.Chat:
					// usefull: v.Type, v.Remote, v.Text
					c := chat.(xmpp.Chat)
					DEFAULT_LOGGER.Print("<<<<<<<<<<<<<<<<<<<<<<<< Received: \n" + pp.Sprint(chat))

					// Discard anything that is not Type "chat" (like "error", "unavailable", ...)
					if c.Type != "chat" {
						continue
					}

					// Discard when there is no text received
					if c.Text == "" {
						continue
					}

					iChatManagerSingleton.receivedChatChan <- c
				case xmpp.Presence:
					DEFAULT_LOGGER.Print("<<<<<<<<<<<<<<<<<<<<<<<< Received: \n" + pp.Sprint(chat))
				}
			}
		}
		go LaunchBaloon_ReceiveChatMessages(iChatManagerSingleton.talk)
	}

	return iChatManagerSingleton, nil
}
func (self *iChatManager) iMessageWaitAndReceive() (receivedText string, receivedChatMessage xmpp.Chat, err error) {
	// blocks until a message comes
	receivedChatMessage, ok := <-self.receivedChatChan
	if !ok {
		return "", xmpp.Chat{}, errors.New("Detected receivedChatChan is closed ?!? Aborting")
	}
	receivedText = receivedChatMessage.Text

	return receivedText, receivedChatMessage, err
}
func (self *iChatManager) iMessageSend(to_remote string, the_message_text string) error {
	// discard /suffixxx abc@def.com/suffixxx, as it was producing strange behaviour in gmail, and its more close to
	// what people expect (receive in *all* devices connected to remote account, not just a *single* device)
	re := regexp.MustCompile("/.*$")
	to_remote = re.ReplaceAllLiteralString(to_remote, "")

	chat_to_send := xmpp.Chat{
		Remote: to_remote,
		Type:   "chat",
		Text:   the_message_text,
	}
	DEFAULT_LOGGER.Print("> > > > > > > > > > > > > Sending: \n" + pp.Sprint(chat_to_send))
	_, err := self.talk.Send(chat_to_send)
	if err != nil {
		DEFAULT_LOGGER.Print("[ERROR] ", err)
		return err
	}

	return nil
}
func (self *iChatManager) iMessageReply(replyText string, previously_receivedChatMessage xmpp.Chat) error {
	to_remote := previously_receivedChatMessage.Remote
	the_message_text := replyText
	err := self.iMessageSend(to_remote, the_message_text)
	if err != nil {
		DEFAULT_LOGGER.Print("[ERROR] ", err)
		return err
	}

	return nil
}

package gmailchatlib

import (
	xmpp "github.com/mattn/go-xmpp"
)

// Init must be called at start of the program
func Init() error {
	_, err := init_iChatManagerSingleton()
	return err
}

// MessageWaitAndReceive reads one message
// The function blocks untill a message is received from any known-contact
// Returns the receivedText of the message, and also the receivedChatMessage object wich contains more related details
func MessageWaitAndReceive() (receivedText string, receivedChatMessage xmpp.Chat, err error) {
	return iChatManagerSingleton.iMessageWaitAndReceive()
}

// MessageSend send a message - containing message_text - to the to_remote contact
// For gmail/hangouts, sometimes the contact is not the same as the gmail-email but a strange-code - to discover that strange-code you can
// receive a messsage from the contact and check in the debug/log messages the strange-code which will come in the "Remote:" field of the
// received message
func MessageSend(to_remote string, the_message_text string) error {
	return iChatManagerSingleton.iMessageSend(to_remote, the_message_text)
}

// MessageReply will reply to (the sender of) a previously-received-message
func MessageReply(replyText string, previously_receivedChatMessage xmpp.Chat) error {
	return iChatManagerSingleton.iMessageReply(replyText, previously_receivedChatMessage)
}

package main

import (
	"fmt"
	"log"
	"os"

	gchat "gitlab.com/zipizap/gmailchatlib"
)

func main() {

	// Define vars and call Init()
	{
		//gchat.DEFAULT_YAML_FILE = "/etc/MyOwnfConfigFile.yaml"		// defaults to "./os.Args[1].yaml"
		gchat.DEFAULT_LOGGER_ENABLED = true // defaults to false
		//gchat.DEFAULT_LOGGER_FILE = "/tmp/MyProg.log"							// defaults to "./os.Args[0].log"
		gchat.DEFAULT_XMPP_STATUSMESSAGE = "Hi from " + os.Args[0] // defaults to "Hi there"
	}
	err := gchat.Init()
	if err != nil {
		log.Fatal(err)
	}

	// This is how to *read* one message (blocks untill a message is received)
	fmt.Println("-------- Waiting to receive a message")
	msgRec_txt, msgRec_chat, err := gchat.MessageWaitAndReceive()
	if err != nil {
		log.Fatal(err)
	}
	sender := msgRec_chat.Remote
	fmt.Println("<< Sender: ", sender)
	fmt.Println("<< Text:   ", msgRec_txt)

	// This is how to *reply* to (the sender of) a previously-received-message
	fmt.Println("\n\n-------- Replying")
	err = gchat.MessageReply("Hi. This is a reply message", msgRec_chat)
	if err != nil {
		log.Fatal(err)
	}

	// This is how to *send* a message, to specific "to_remote"
	// NOTE: get these strange remote contacts from the log file... no other easy way... sometimes its "username@gmail.com" sometimes its these strange codes...
	fmt.Println("-------- Sending a message to a specific contact")
	a_known_contact := "30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com"
	err = gchat.MessageSend(a_known_contact, "Hey, this is a sent message")
	if err != nil {
		log.Fatal(err)
	}

	// This is how to *reuse the logger*
	gchat.DEFAULT_LOGGER.Print("[myProg - ERROR] If DEFAULT_LOGGER_ENABLED == true then this text will be logged by DEFAULT_LOGGER and saved into DEFAULT_LOGGER_FILE")

}

package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"github.com/fatih/color"
	xmpp "github.com/mattn/go-xmpp"
	flag "github.com/spf13/pflag"
	gchat "gitlab.com/zipizap/gmailchatlib"
)

var flag_infiniteloop bool
var flag_logger_enable bool

func initFlags() {
	flag.BoolVarP(&flag_infiniteloop, "infiniteloop", "i", false, "loop infinitely")
	flag.BoolVarP(&flag_logger_enable, "log", "l", false, "create log file")
	flag.Parse()
	if flag_logger_enable == true {
		gchat.DEFAULT_LOGGER_ENABLED = true
	}
}

func main() {
	initFlags()

	// Process args and if any missing show usage&quit
	var usercommand string
	{
		if len(flag.Args()) != 1 {
			fmt.Printf(`
      %s [-i|--infiniteloop] [-l|--log] <USERCOMMAND>
 
      This program will wait-until-receives a chat message (from any known contact), and then execute the USERCOMMAND like:
          USERCOMMAND <MsgSender> <MsgText>

      The USERCOMMAND can be any script/command
			The program will only run once (receive 1 message > exec USERCOMMAND), but if -i|--infiniteloop is present it will loop infinitely
			The -l|--log flag activates logging to a file in same dir where program is running. Beware log files can become big
     

  
    EXAMPLES:

      gchat-receiveAndReply$ ls -l
      total 7312
      -rwxrwxr-x 1 paulo paulo      42 Sep 19 22:23 convert_to_hex.sh
      -rwxrwxr-x 1 paulo paulo 7451833 Sep 21 11:46 gchat-receiveAndReply
      -rw-rw-r-- 1 paulo paulo    3300 Sep 21 11:44 gchat-receiveAndReply.go
      -rw------- 1 paulo paulo    4593 Sep 21 11:57 gchat-receiveAndReply.log
      -rw-rw-r-- 1 paulo paulo     217 Sep 21 11:46 gchat-receiveAndReply.yaml
      -rwxrwxr-x 1 paulo paulo      36 Sep 21 11:49 reverse.sh
      -rwxrwxr-x 1 paulo paulo      90 Sep 21 11:11 run_in_bash.sh
      -rwxrwxr-x 1 paulo paulo      48 Sep 21 11:56 show_Remote_contact_strange-code.sh


      $ ./gchat-receiveAndReply ./reverse.sh
      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Waiting to receive next message...
      SENDER: 30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com/lcsw_hangouts_D3FED85C
      TEXT:
      Hi - this is a message sent into the bot
      .............................. Calling usercommand <MsgSender> <MsgText> ...
      USERCOMMAND :
      './reverse.sh' '30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com/lcsw_hangouts_D3FED85C' 'Hi - this is a message sent into the bot'
      STDOUTSTDERR:
      tob eht otni tnes egassem a si siht - iH
      >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Replying with stdoutStderr of USERCOMMAND...
      TO  : 30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com/lcsw_hangouts_D3FED85C
      TEXT:
      tob eht otni tnes egassem a si siht - iH

      
      gchat-receiveAndReply$ ./gchat-receiveAndReply ./show_Remote_contact_strange-code.sh
      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Waiting to receive next message...
      SENDER: 30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com/lcsw_hangouts_D3FED85C
      TEXT:
      me
      .............................. Calling usercommand <MsgSender> <MsgText> ...
      USERCOMMAND :
      './show_Remote_contact_strange-code.sh' '30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com/lcsw_hangouts_D3FED85C' 'me'
      STDOUTSTDERR:
      30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com
      >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Replying with stdoutStderr of USERCOMMAND...
      TO  : 30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com/lcsw_hangouts_D3FED85C
      TEXT:
      30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com





`, os.Args[0])
			os.Exit(1)
		}
		// At this point: args considered all ok, lets use them
		usercommand = string(flag.Args()[0])
	}

	// init (once)
	{
		gchat.DEFAULT_XMPP_STATUSMESSAGE = "Receive-and-reply with '" + usercommand + "'"
		err := gchat.Init()
		if err != nil {
			log.Fatal(err)
		}
	}

	// setup colors
	color.NoColor = false
	hiblue := color.New(color.FgHiBlue).SprintFunc()
	yellow := color.New(color.FgYellow).SprintFunc()

	for {
		// read message (blocks untill a message is received)
		var msgRec_txt string
		var msgRec_chat xmpp.Chat
		var msgRec_sender string
		{
			fmt.Println(yellow("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Waiting to receive next message..."))
			var err error
			msgRec_txt, msgRec_chat, err = gchat.MessageWaitAndReceive()
			if err != nil {
				log.Fatal(err)
			}
			msgRec_sender = msgRec_chat.Remote
			fmt.Println(hiblue("SENDER:\t") + msgRec_sender)
			fmt.Println(hiblue("TEXT:\n") + msgRec_txt)
		}

		// call external usercommand with arg1=msgRec_sender arg2=msgRec_txt
		var usercommand_stdoutStderr string
		{
			fmt.Println(yellow(".............................. Calling usercommand <MsgSender> <MsgText> ..."))
			cmd := exec.Command(usercommand, msgRec_sender, msgRec_txt)
			usercommand_stdoutStderr_bytes, err := cmd.CombinedOutput()
			if err != nil {
				usercommand_stdoutStderr = "Error: " + err.Error() + "\n"
			} else {
				usercommand_stdoutStderr = string(usercommand_stdoutStderr_bytes)
			}
			fmt.Println(hiblue("USERCOMMAND :\n")+"'"+usercommand+"'", "'"+msgRec_sender+"'", "'"+msgRec_txt+"'")
			fmt.Print(hiblue("STDOUTSTDERR:\n") + usercommand_stdoutStderr)
		}

		// reply to (sender of) a previously-received-chatmessage
		{
			fmt.Println(yellow(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Replying with stdoutStderr of USERCOMMAND..."))
			err := gchat.MessageReply(usercommand_stdoutStderr, msgRec_chat)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(hiblue("TO  :"), msgRec_sender)
			fmt.Print(hiblue("TEXT:\n") + usercommand_stdoutStderr + "\n\n\n")
		}

		if !flag_infiniteloop {
			break
		}
	}
}

#!/usr/bin/env bash
for an_example in $(find $PWD/* -type d); do echo "Building $an_example..."; cd $an_example && go build ; done
rm -fv ReceiveReplySend/ReceiveReplySend

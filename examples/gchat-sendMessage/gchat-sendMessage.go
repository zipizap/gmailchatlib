package main

import (
	"fmt"
	"log"
	"os"

	gchat "gitlab.com/zipizap/gmailchatlib"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Printf(`
  USAGE: 
    %s <RECEIVER_CONTACT> <MESSAGE_TEXT>
  
  EXAMPLE: 
    %s 30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com "Hi there!"
    %s userY@gmail.com "Hi there!"

  NOTES:
    + <RECEIVER_CONTACT> cannot be a new contact, must be someone you already chatted before using hangouts/gmail-webpage
    + See https://gitlab.com/zipizap/gmailchatlib
`, os.Args[0], os.Args[0], os.Args[0])
		os.Exit(1)
	}

	// init (once)
	err := gchat.Init()
	if err != nil {
		log.Fatal(err)
	}

	receiver_contact := string(os.Args[1])
	message_text := string(os.Args[2])
	fmt.Println("-------- Sending message")
	fmt.Println("TO:\t" + receiver_contact)
	fmt.Println("TEXT:")
	fmt.Println(message_text)
	err = gchat.MessageSend(receiver_contact, message_text)
	if err != nil {
		log.Fatal(err)
	}

}

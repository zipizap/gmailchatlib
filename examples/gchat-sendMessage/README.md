gchat-sendMessage: cli tool to send gmail chat messages easily

Works between `gmail` accounts that successfully chatted before 



# INSTALL

Choose one of the options bellow

## Download binary and yaml-config 

    # Download gchat-sendMessage binary
    wget 'https://gitlab.com/zipizap/gmailchatlib/raw/master/examples/gchat-sendMessage/gchat-sendMessage'

    # Download gchat-sendMessage.yaml config file
    wget 'https://gitlab.com/zipizap/gmailchatlib/raw/master/examples/gchat-sendMessage/gchat-sendMessage.yaml'

    # Set binary executable
    chmod +x gchat-sendMessage

		# Final check
		$ ls -la
		total 9456
		-rwxrwxr-x  1 userX userX 9650992 sep 22 23:31 gchat-sendMessage
		-rw-rw-r--  1 userX userX     213 sep 22 23:31 gchat-sendMessage.yaml



## Or compile youself from go source-code

    # Compile gchat-sendMessage binary
    go get -u gitlab.com/zipizap/gmailchatlib/examples/gchat-sendMessage
    go build gitlab.com/zipizap/gmailchatlib/examples/gchat-sendMessage
  
    # Copy gchat-sendMessage.yaml config file
    cp $GOPATH/src/gitlab.com/zipizap/gmailchatlib/examples/gchat-sendMessage/gchat-sendMessage.yaml .

    # Final check
    $ ls -la
    total 9456
    -rwxrwxr-x  1 userX userX 9650992 sep 22 23:31 gchat-sendMessage
    -rw-rw-r--  1 userX userX     213 sep 22 23:31 gchat-sendMessage.yaml




# CONFIGURE

## Confirm chat is successfull between 2 gmail accounts

This program is only able to communicate with other gmail "known" contacts

You need 2 gmail accounts - serverX@gmail.com anneY@gmail.com

In one browser, open gmail/hangouts with the serverX account. In another browser, open gmail/hangouts with the anneY account. Manually type and verify you can send and receive chat messages between the 2 accounts - this is necessary to assure that between the 2 accounts there are no invitations pending and both are "known" to each-other. Or else the program won't be able to communicate


## Edit the yaml file with the serverX account details

Edit the yaml file:


    account:
      server_and_port: "talk.google.com:443"
      username: 'serverX@gmail.com
      password: 'gmailPasswordHereForServerXaccount'


# USAGE
  
    $ ./gchat-sendMessage 

    USAGE: 
      ./gchat-sendMessage <RECEIVER_CONTACT> <MESSAGE_TEXT>
    
    EXAMPLE: 
      ./gchat-sendMessage 30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com "Hi there!"
      ./gchat-sendMessage userY@gmail.com "Hi there!"

    NOTES:
      + <RECEIVER_CONTACT> cannot be a new contact, must be someone you already chatted before using hangouts/gmail-webpage
      + See https://gitlab.com/zipizap/gmailchatlib





# USEFULL NOTES AND LIMITATIONS

This program was made using the gmailchatlib go library, and inherits the same "USEFULL NOTES AND LIMITATIONS" of https://gitlab.com/zipizap/gmailchatlib

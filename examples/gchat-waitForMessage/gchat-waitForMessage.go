package main

import (
	"fmt"
	"log"
	"time"

	gchat "gitlab.com/zipizap/gmailchatlib"
)

func main() {

	// init (once)
	err := gchat.Init()
	if err != nil {
		log.Fatal(err)
	}

	// read message (blocks untill a message is received)
	fmt.Println("... Waiting to receive a message...")
	msgRec_txt, msgRec_chat, err := gchat.MessageWaitAndReceive()
	if err != nil {
		log.Fatal(err)
	}

	sender := msgRec_chat.Remote
	fmt.Println("================ ", time.Now().Local())
	fmt.Println("SENDER:\t" + sender)
	fmt.Println("TEXT:\n" + msgRec_txt)

}

# GmailChatLib

https://gitlab.com/zipizap/gmailchatlib

http://godoc.org/gitlab.com/zipizap/gmailchatlib

This lib is a minimalistic xmpp/jabber client, containing only a few functions to send, receive and reply messages from known contacts

The lib will connect and use one gmail-account (xmpp/jabber) to communicate. That account must be configured by editing a .yaml file (named "./os.Args[0].yaml" by default, but configurable)

The lib implements a logger (log.Logger) that when enabled (by default disabled) will save the xmpp messages exchanged to a logfile (named "./os.Args[0].log", but configurable). That logger can be reused by your program

This lib is not a full-fledged chat-client, it only has 3 functions and does not support many other operations (such as send/accept invites, files, status changees, etc)
Its purposedly simple, to be easy to use - mainly a varnish-coat on top of the great prior work of github.com/mattn/go-xmpp

Although this lib was made and tested with a xxxx@gmail.com account, it (theoretically) could work with other xmpp/jabber-provider.  
You could try one of: https://list.jabber.at/ and report success/failure in https://gitlab.com/zipizap/gmailchatlib as an issue, so other people known about it. A resume of all reports is kept in https://gitlab.com/zipizap/gmailchatlib/wikis/Other-public-XMPP-Jabber-providers



--------------------


DEMO

You need 2 gmail accounts - serverX@gmail.com anneY@gmail.com

In one browser, open gmail/hangouts with the serverX account.
In another browser, open gmail/hangouts with the anneY account.
Manually type and verify you can send and receive chat messages between the 2 accounts - this is necessary to assure that between the 2 accounts there are no inivitations pending and both are "known" to each-other. Or else the lib won't be able to communicate



Create myProg.yaml with:

	account:
	  server_and_port: "talk.google.com:443"
	  username: 'serverX@gmail.com
	  password: 'gmailPasswordHereForServerXaccount'


Create myProg.go with:

	package main
	import (
		"fmt"
		"log"
		"os"
		gchat "gitlab.com/zipizap/gmailchatlib"
	)

	func main() {

		// Define vars and call Init()
		{
			//gchat.DEFAULT_YAML_FILE = "/etc/MyOwnfConfigFile.yaml"    // defaults to "./os.Args[1].yaml"
			gchat.DEFAULT_LOGGER_ENABLED = true                         // defaults to false
			//gchat.DEFAULT_LOGGER_FILE = "/tmp/someName.log"           // defaults to "./os.Args[0].log"
			gchat.DEFAULT_XMPP_STATUSMESSAGE = "Hi from " + os.Args[0]  // defaults to "Hi there"
		}
		err := gchat.Init()
		if err != nil {
			log.Fatal(err)
		}

		// This is how to *read* one message (blocks untill a message is received)
		fmt.Println("-------- Waiting to receive a message")
		msgRec_txt, msgRec_chat, err := gchat.MessageWaitAndReceive()
		if err != nil {
			log.Fatal(err)
		}
		sender := msgRec_chat.Remote
		fmt.Println("<< Sender: ", sender)
		fmt.Println("<< Text:   ", msgRec_txt)

		// This is how to *reply* to (the sender of) a previously-received-message
		fmt.Println("\n\n-------- Replying")
		err = gchat.MessageReply("Hi. This is a reply message", msgRec_chat)
		if err != nil {
			log.Fatal(err)
		}

		// This is how to *send* a message, to specific "to_remote"
		// NOTE: get these strange remote contacts from the log file... no other easy way... sometimes its "username@gmail.com" sometimes its these strange codes...
		fmt.Println("-------- Sending a message to a specific contact")
		a_known_contact := "anneY@gmail.com"
		//a_known_contact := "30nqs9jkinkyq14x9o4d4jj3ii@public.talk.google.com"
		err = gchat.MessageSend(a_known_contact, "Hey, this is a sent message")
		if err != nil {
			log.Fatal(err)
		}

		// This is how to *reuse the logger*
		gchat.DEFAULT_LOGGER.Print("[myProg - ERROR] If DEFAULT_LOGGER_ENABLED == true then this text will be logged by DEFAULT_LOGGER and saved into DEFAULT_LOGGER_FILE")

	}


Now, do:

	go run myProg.go

Manually goto the browser open with gmail/hangouts of anneY@gmail.com and write a chat message to serverX@gmail.com.

The myProg.go will *receive* that message and shown it in terminal

Then myProg.go will *reply* with a message to anneY which you should see in the browser

And after myProg.go will *send* a message to a specific address (which is anneY@gmail.com in this demo, but could by any known-contact of serverX)

Then myProg.go will terminate, and you can check the created log file:

	cat myProg.log



See more examples in https://gitlab.com/zipizap/gmailchatlib/tree/master/examples

------------------------------



DEFAULT_LOGGER

DEFAULT_LOGGER is the log.Logger object that stores log entries

The logger is disabled by default, but you can enable it, and optionally indicate the log-file, by setting these variables before calling Init():


	gmailchatlib.DEFAULT_LOGGER_ENABLED = true
	gmailchatlib.DEFAULT_LOGGER_FILE = "/tmp/MyProg.log"
	gmailchatlib.Init()


Also, if you want to add your own log messages to the logger, you can do so like this:

	//in the middle of your program code
	gmailchatlib.DEFAULT_LOGGER.Print("[myProg - ERROR] If DEFAULT_LOGGER_ENABLED == true then this text will be logged by DEFAULT_LOGGER and saved into DEFAULT_LOGGER_FILE")


When a previous DEFAULT_LOGGER_FILE already exists, it gets overwritten. Also, beware that DEFAULT_LOGGER_FILE might grow quickly in size and become quite big (I saw 3GB in 1 day without much activity...), so probably its best for you to enable it carefully only when needed



------------------------------


USEFULL NOTES AND LIMITATIONS

- This lib does not implement send-or-receive invitations for new contacts

So to add a new contact, you must *manually* go to the hangouts/gmail webpage of serverX, and *invite* new contact, and *accept* invitation from new contact and successfully send-and-receive messages from the new contact (in webpage)

Only after the invitation+messages-exchange is successfull with a new contact (manually), only then this lib will be able to send/receive messages to that contact


- MessageSend() function needs to have as argument a "to_remote" address.

Sometimes the someuser@gmail.comi normal-address will not work and instead a "strange-address" (ex: 30nqsnkyq14x9o4d4jj3ii@public.talk.google.com) needs to be used instead

I'm not aware how to get the "strange-address" and I've seen other more-famous-lib that also didnt handle it, so  my guess is that its probably not easy and out-of-reach.

So, the nly way to get the "strange-address" is to set DEFAULT_LOGGER_ENABLED=true and manually look into the .log file to get it from there.

So this lib is not very usefull if you plan to chat with a lot of new contacts, but its handy if you will only chat with a handfull of contacts (still beareable to check their addresses manually)

- Gtalk does not support XMPP-Server2Server, so you can only communicate with other xxx@gmail.com but not with any yyy@NOTgmail.com

- As happens in many things of life, this project is a tradeoff of time vs features. It got to a point where its "good enough" for what I want to do, but it still could be improved much much more. Feel free to fork it and improve it (GPL3)


------------------------------
